function cb() {
    return "Callback function is invoked";
}
function limitFunctionCallCount(cb, n) {
    if (cb === undefined || n === undefined || typeof (n) !== 'number' || typeof(cb) !== 'function') {
        return null;
    }
    let count = n;
    function invoke() {
        if (count > 0) {
            count--;
            let callBack = cb();
            return callBack;
        } else {
            return null;
        }
    }
    return invoke;
}
module.exports.limitFunctionCallCount = limitFunctionCallCount;
module.exports.cb = cb; 