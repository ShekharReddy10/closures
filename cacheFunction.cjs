function cacheFunction(cb) {
    if (typeof (cb) != 'function' || cb === undefined) {
        return null;
    }
    let cache = {};
    function invokeCb(...param) {
        if (cache.hasOwnProperty(param)) {
            return cache;

        } else {

            cache[param] = param;
            let callBack = cb();
            return callBack;
        }
    }
    return invokeCb;
}
function cb() {
    return `callback is invoked`
}
module.exports.cb = cb;
module.exports.cacheFunction = cacheFunction;