function counterFactory() {
    let counter = 0;
    let incrementDecrementobj = {
        increment(){
            return ++counter;
        },
        decrement() {
            return --counter;
        }
    }
    return incrementDecrementobj ;
}
module.exports = counterFactory;